from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# Setup
base_url = 'http://localhost/mutillidae'
browser = webdriver.Firefox()
browser.implicitly_wait(3)

browser.get(base_url)

browser.quit()
